import { Link } from "gatsby";
import React, { FunctionComponent } from "react";


interface ComponentProps {
  siteTitle: string;
}

const Header: FunctionComponent<ComponentProps> = ({ siteTitle }) => (
  <header>
    <div>
      <h1>
        <Link
          to="/"
          style={{
            color: `white`,
            textDecoration: `none`,
          }}
        >
          {siteTitle}
        </Link>
      </h1>
    </div>
  </header>
)

export default Header;
