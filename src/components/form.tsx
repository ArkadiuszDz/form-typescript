import React, { FunctionComponent } from "react";
import { FormProps, FormDispatch } from '../containers/form';
import { DeepReadonly } from 'ts-essentials';
import { FormInput, SelectOption } from '../logic/form/store';

import '../styles/form.scss';

interface ComponentProps
  extends
    FormProps,
    FormDispatch,     
    DeepReadonly<{}> {}


const Form: FunctionComponent<ComponentProps> = ({formValues, inputs, optionalInputs, select, setFormData}) => {
  
  const inputHandler = (e: any) => {
    e.preventDefault();
    setFormData({
      ...formValues,
      [e.target.name]: e.target.value
    });
  }


  return (
    <div>
      <form>
        {
          inputs &&
          inputs.map((element: FormInput, index: number) => {
            return (
              <div className="input-wrapper" key={`${element.name}-${index}`}>
                <label>{element.label}</label>
                <input 
                  type={element.type}
                  name={element.name}
                  onChange={inputHandler}
                  // @ts-ignore
                  value={formValues[element.name]}
                />
              </div>
            )
          })
        }
        {
          select &&
          select.options &&   
          <div className="input-wrapper select-wrapper">
            <select 
              name={select.name}
               // @ts-ignore
              value={formValues[select.name]} 
              onChange={inputHandler}
            >
              <option value="" hidden>Choose dish type</option>
              {
                select.options.map((element: SelectOption, index: number) => {
                  return (
                    <option value={element.value} key={`${element.value}-${index}`}>{element.label}</option>
                  )
                })
              }
            </select>
          </div>
        }
        {
          optionalInputs &&
          // @ts-ignore
          optionalInputs[formValues[select.name]] &&
           // @ts-ignore
          optionalInputs[formValues[select.name]].map((element: FormInput, index: number) => {
            return (
              <div className="input-wrapper" key={`optional-${element}-${index}`}>
                <label>{element.label}</label>
                <input
                  type={element.type}
                  name={element.name}
                  // @ts-ignore
                  value={formValues[element.name]}
                  onChange={inputHandler}
                />
              </div>
            )
          })
        }
        <div className="btn-wrapper">
          <div className="btn">
            <button>
              Show Modal
            </button>
          </div>
        </div>
      </form>
    </div>
  )
}

export default Form;
