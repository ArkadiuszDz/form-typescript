import React, { FunctionComponent } from "react";
import { DeepReadonly } from 'ts-essentials';
import { ModalProps, ModalDispatch } from '../containers/modal';

import '../styles/modal.scss';

interface ComponentProps
  extends
    ModalProps,
    ModalDispatch,
    DeepReadonly<{}> {}

const Modal: FunctionComponent<ComponentProps> = ({ visibleModal }) => {
  console.log(visibleModal);
  return (
    <>
      {
        visibleModal &&
        <div className="modal">
          <div className="modal--inner">
            
          </div>
        </div>
      }
    </>
  )
}

export default Modal;
