/**
 * Layout component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.org/docs/use-static-query/
 */

import React, { FunctionComponent } from "react";
import { useStaticQuery, graphql } from "gatsby";
import  { Provider } from 'react-redux';
import { store } from '../logic/configure-store';
import { getFormData } from '../logic/form/actions';

import Header from "./header";
import "../styles/layout.scss";

interface ComponentProps {
  children: React.ReactNode;
}

const formStore = {
  formValues: {
    dishName: '',
    dishType: '',
    prepTime: '',
    spicynessScale: 0,
    noOfSlices: 0,
    diameter: '',
    slicesOfBread: 0
  },
  inputs: [
    {
      name: 'dishName',
      type: 'text',
      label: 'Dish name'
    },
    {
      name: 'prepTime',
      type: 'text',
      label: 'Preparation time'
    }
  ],
  select: {
    name: "dishType",
    options: [
      {
        value: "pizza",
        label: "Pizza"
      },
      {
        value: "soup",
        label: "Soup"
      },
      {
        value: "sandwich",
        label: "Sandwich"
      }
    ]
  },
  optionalInputs: {
    soup: [
      {
        name: 'spicynessScale',
        type: 'number',
        label: 'Spicyness Scale'
      }
    ],
    pizza: [
      {
        name: 'noOfSlices',
        type: 'number',
        label: 'Number of slices'
      },
      {
        name: 'diameter',
        type: 'text',
        label: 'Diameter'
      }
    ],
    sandwich: [
      {
        name: 'slicesOfBread',
        type: 'number',
        label: 'Slices of bread'
      }
    ]
  }
};


const Layout: FunctionComponent<ComponentProps> = ({ children }) => {
  const data = useStaticQuery(graphql`
    query SiteTitleQuery {
      site {
        siteMetadata {
          title
        }
      }
    }
  `)
  store.dispatch(getFormData(formStore));
  return (
    <Provider store={store}>
      <Header siteTitle={data.site.siteMetadata.title} />
      <div>
        <main>
          {children}
        </main>
        <footer>
        </footer>
      </div>
    </Provider>
  )
}

export default Layout;
