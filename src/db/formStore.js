export const formStore = {
  formValues: {
    dishName: '',
    dishType: '',
    prepTime: '',
    spicynessScale: 0,
    noOfSlices: 0,
    diameter: '',
    slicesOfBread: 0
  },
  inputs: [
    {
      name: 'dishName',
      type: 'text',
      label: 'Dish name'
    },
    {
      name: 'prepTime',
      type: 'text',
      label: 'Preparation time'
    }
  ],
  select: {
    name: "dishType",
    options: [
      {
        value: "pizza",
        label: "Pizza"
      },
      {
        value: "soup",
        label: "Soup"
      },
      {
        value: "sandwich",
        label: "Sandwich"
      }
    ]
  },
  optionalInputs: {
    soup: [
      {
        name: 'spicynessScale',
        type: 'number',
        label: 'Spicyness Scale'
      }
    ],
    pizza: [
      {
        name: 'noOfSlices',
        type: 'number',
        label: 'Number of slices'
      },
      {
        name: 'diameter',
        type: 'text',
        label: 'Diameter'
      }
    ],
    sandwich: [
      {
        name: 'slicesOfBread',
        type: 'number',
        label: 'Slices of bread'
      }
    ]
  }
};

