import React from "react";

import Layout from "../components/layout";
import { SEO } from "../components/seo";
import { FormContainer } from '../containers/form';
import { ModalContainer } from "../containers/modal";


const IndexPage = () => {
  return (
    <Layout>
      <SEO title="Home" />
      <ModalContainer />
      <FormContainer />
    </Layout>
  );
}

export default IndexPage;
