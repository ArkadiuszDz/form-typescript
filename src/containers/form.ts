import { connect } from 'react-redux';
import { Dispatch } from 'redux';

// Components
import Form from '../components/form';

// Logic & Store & Selectors & Helpers
import { RootStore } from '../logic/root-store';
import { FormDataStore, FormData } from '../logic/form/store';
import { getFormData, getFormInputs, getFormOptionalInputs, getFormSelect } from '../logic/form/selectors';
import { setFormData } from '../logic/form/actions';
import { setModalVisible } from '../logic/modal/actions';
import { ModalStore } from '../logic/modal/store';

// Define data typefor container based on store part
export interface FormProps
  extends Pick<FormDataStore, 'formValues' | 'inputs' | 'optionalInputs' | 'select'> {}


// Define data type for actions which change store
export interface FormDispatch {
  setFormData: (data: FormData) => void;
  setModalVisible: (visible: ModalStore) => void;
}

// Select store parts
const mapStateToProps = (state: RootStore): FormProps => ({
  formValues: getFormData(state),
  inputs: getFormInputs(state),
  optionalInputs: getFormOptionalInputs(state),
  select: getFormSelect(state)
});

// Dispatch actions which change store
const mapDispatchToProps = (dispatch: Dispatch): FormDispatch => ({
  setFormData: (formValues: FormData) => dispatch(setFormData(formValues)),
  setModalVisible: (visible: ModalStore) => dispatch(setModalVisible(visible))
});

export const FormContainer = connect<
  FormProps,
  FormDispatch,
  {},
  RootStore
>(
  mapStateToProps,
  mapDispatchToProps
)(Form);
