import { connect } from 'react-redux';
import { Dispatch } from 'redux';

import Modal from '../components/modal';


// Logic & Store & Selectors & Helpers
import { RootStore } from '../logic/root-store';
import { ModalStore } from '../logic/modal/store';
import { getModal} from '../logic/modal/selectors';
import { setModalVisible } from '../logic/modal/actions';

// Define data typefor container based on store part
export interface ModalProps
  extends Pick<ModalStore, 'visibleModal'> {}

// Define data type for actions which change store
export interface ModalDispatch {
  setModalVisible: (data: ModalStore) => void;
}

// Select store parts
const mapStateToProps = (state: RootStore): ModalProps => ({
  visibleModal: getModal(state),
});

// Dispatch actions which change store
const mapDispatchToProps = (dispatch: Dispatch): ModalDispatch => ({
  setModalVisible: (visible: ModalStore) => dispatch(setModalVisible(visible))
});

export const ModalContainer = connect<
  ModalProps,
  ModalDispatch,
  {},
  RootStore
>(
  mapStateToProps,
  mapDispatchToProps
)(Modal);
