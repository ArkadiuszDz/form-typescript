import { RootStore } from './root-store';
import { combineReducers } from 'redux';

// Import single reducers
import { FormDataReducer } from './form/redux';
import { ModalReducer } from './modal/redux';

export const rootReducer = combineReducers<RootStore>({
  formData: FormDataReducer,
  modal: ModalReducer
});
