import { RootStore } from './root-store';

// Import initial stores
import { FormDataInitialStore } from './form/redux';
import { ModalInitialStore } from './modal/redux';

export const initialState: RootStore = {
  formData: FormDataInitialStore,
  modal: ModalInitialStore
};
