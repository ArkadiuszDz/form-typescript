import { FormDataStore } from './store';
import { Action } from 'redux';
import { constants } from './actions';


// Initial store
export const FormDataInitialStore: FormDataStore = {
  formValues: {
    dishName: '',
    dishType: '',
    prepTime: '',
    spicynessScale: 0,
    noOfSlices: 0,
    diameter: '',
    slicesOfBread: 0
  },
  inputs: [],
  select: {
    name: '',
    options: []
  },
  optionalInputs: {}
};


export function FormDataReducer(state = FormDataInitialStore, action: Action) {
  switch (action.type) {

    case constants.GET_FORM_DATA:
      return {
        ...state,
          // @ts-ignore
        inputs: action.data.inputs,
        // @ts-ignore
        formValues: action.data.formValues,
        // @ts-ignore
        optionalInputs: action.data.optionalInputs,
        // @ts-ignore
        select: action.data.select
      }

    case constants.SET_FORM_DATA:
      return {
        ...state,
        // @ts-ignore
        formValues: action.formValues
      };

    case constants.GET_FORM_OPTIONAL_INPUTS:
      return {
        ...state,
            // @ts-ignore
        optionalInputs: action.optionalInputs
      }

    default:
      return state;
  }
}
