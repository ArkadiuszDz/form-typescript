import { RootStore } from '../root-store';
import { FormDataStore } from './store';

export const getFormData = (state: RootStore): FormDataStore['formValues'] => state.formData.formValues;
export const getFormInputs = (state: RootStore): FormDataStore['inputs'] => state.formData.inputs;
export const getFormOptionalInputs = (state: RootStore): FormDataStore['optionalInputs'] => state.formData.optionalInputs;
export const getFormSelect = (state: RootStore): FormDataStore['select'] => state.formData.select;