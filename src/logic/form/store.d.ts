import { DeepReadonly } from 'ts-essentials';

interface FormData {
  dishName: string;
  dishType: string;
  prepTime: string;
  spicynessScale?: number;
  noOfSlices?: number;
  diameter?: string;
  slicesOfBread?: number;
}

interface FormInput {
  name: string;
  label: string;
  type: string;
}

interface SelectOption {
  value: string;
  label: string;
}

interface SelectInput {
  name: string;
  options: SelectOption[];
}

interface OptionalInputs {
  [type]: FormInput[]
}


export interface FormDataStore
  extends DeepReadonly<{
    formValues: FormData;
    inputs: FormInput[];
    select: SelectInput;
    optionalInputs: OptionalInputs;
  }> {}
