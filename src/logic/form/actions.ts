import { FormData, FormDataStore } from "./store";

// Declare - Constants
export const constants = {
  GET_FORM_DATA: 'GET_FORM_DATA',  // get all inputs values and inputs
  SET_FORM_DATA: 'SET_FORM_DATA',   // set all inputs values
  GET_FORM_OPTIONAL_INPUTS: 'GET_FORM_OPTIONAL_INPUTS'  // get form optional inputs
};


export const getFormData = (data: FormDataStore) => ({
  type: constants.GET_FORM_DATA,
  data
});

export const setFormData = (formValues: FormData) => ({
  type: constants.SET_FORM_DATA,
  formValues
});


export const getFormOptionalInputs = (optionalInputs: string) => ({
  type: constants.GET_FORM_OPTIONAL_INPUTS,
  optionalInputs // make this asynchronous
});