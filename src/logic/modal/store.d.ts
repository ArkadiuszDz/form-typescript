import { DeepReadonly } from 'ts-essentials';


export interface ModalStore
  extends DeepReadonly<{
    visibleModal: boolean;
  }> {}
