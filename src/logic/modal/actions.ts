import { ModalStore } from './store';

// Declare - Constants
export const constants = {
  GET_MODAL_DATA: 'GET_MODAL_DATA',
  SET_MODAL_DATA: 'SET_MODAL_DATA'
};

export const setModalVisible = (visible: ModalStore) => ({
  type: constants.SET_MODAL_DATA,
  visible
})