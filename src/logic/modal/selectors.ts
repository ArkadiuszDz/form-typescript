import { RootStore } from '../root-store';
import { ModalStore } from './store';

export const getModal = (state: RootStore): ModalStore['visibleModal'] => state.modal.visibleModal;