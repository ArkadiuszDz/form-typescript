import { ModalStore } from './store';
import { Action } from 'redux';
import { constants } from './actions';


// Initial store
export const ModalInitialStore: ModalStore = {
  visibleModal: false
};


export function ModalReducer(state = ModalInitialStore, action: Action) {
  switch (action.type) {

    case constants.SET_MODAL_DATA:
      return {
        ...state,
            // @ts-ignore
        visibleModal: action.visible
      }

    default:
      return state;
  }
}
