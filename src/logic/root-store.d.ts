import { DeepReadonly } from 'ts-essentials';

// Import single store
import { FormDataStore } from './form/store';
import { ModalStore } from './modal/store';

export interface RootStore
  extends DeepReadonly<{
    formData: FormDataStore;
    modal: ModalStore;
  }> {}
